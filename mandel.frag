// https://syntopia.github.io/Fragmentarium/usage.html
#include "2D.frag"
#group Simple Mandelbrot


// Creates a sliders.  The values in the brackets are minimum, default, and maximum values.

// maximal number of iterations
uniform int iMax; slider[1,10,1000]
// er2= er^2 wher er= escape radius = bailout
uniform float er2; slider[4.0,4.0,1000]   

// compute color of pixel
 vec3 color(vec2 c) {
   vec2 z = vec2(0.0);  // initial value

 // iteration
  for (int i = 0; i < iMax; i++) {
    z = vec2(z.x*z.x-z.y*z.y, 2*z.x*z.y) +  c; // z= z^2+c
    if (dot(z,z)> er2)   // escape test 
      return vec3(1.0- float(i)/float(iMax)); // exterior
    
  }
  return vec3(0.0); //interior
}
