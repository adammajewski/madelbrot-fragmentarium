#include "2D.frag"
#group Simple Mandelbrot

// maximal number of iterations
uniform int iMax; slider[1,100,1000] 
uniform float er2; slider[4.0,1000.0,10000.0] 

// compute color of pixel
 vec3 color(vec2 c) {
   vec2 z = vec2(0.0);  // initial value

 // iteration
  for (int i = 0; i < iMax; i++) {
    z = vec2(z.x*z.x-z.y*z.y,2*z.x*z.y) +  c; // z= z^2+c
    if (dot(z,z)> er2)   // escape test 
      // exterior
      if (z.x>0){ return vec3( 1.0);} // upper part of the target set 
      else return vec3(0.0); //lower part of the target set 
  }
  return vec3(0.0); //interior
}
